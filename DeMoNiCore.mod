<?xml version="1.0" encoding="UTF-8"?>
<!--
********************************************************************************
DeMoNiCore by DeMoN (Ken Hoffmeyer, demon AT demonicstudios DOT com)

This addon, and all original code contained in the files listed below at a), 
is copyright by Ken Hoffmeyer with all rights reserved.  The code may not be
reused or modified without express consent of the author.  You may create new
original addons that make use of this addon, however you may NOT create an addon
that uses it to circumvent the Destruction-only nature of other addons created
by the author (AntiHealer, AntiWP).

a) List of copyrighted files
DeMoNiCore.lua
DeMoNiCore.mod
********************************************************************************
-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <UiMod name="DeMoNiCore" version="1.3.51" date="08/06/2010">
    <Author name="DeMoN" email="demon@demonicstudios.com" />
    <Description text="Parsing framework (model in MVC) for my other mods to observe." />
	<VersionSettings gameVersion="1.3.5" windowsVersion="1.0" savedVariablesVersion="1.3.5.1r" />
    
	<Dependencies>
	  	<Dependency name="EASystem_Strings" />
    </Dependencies>
    
	<Files>
	  <File name="DeMoNiCore.lua" />
    </Files>
		
    <OnInitialize>
      <CallFunction name="DeMoNiCore.OnInitialize" />
    </OnInitialize>
	
    <OnUpdate />
	
    <OnShutdown>
      <CallFunction name="DeMoNiCore.OnShutdown" />
    </OnShutdown>
	
    <WARInfo>
      <Categories>
        <Category name="RVR" />
      </Categories>
      <Careers>
        <Career name="BLACK_ORC" />
        <Career name="BLACKGUARD" />
        <Career name="CHOPPA" />
        <Career name="CHOSEN" />
        <Career name="DISCIPLE" />
        <Career name="MAGUS" />
        <Career name="MARAUDER" />
        <Career name="SHAMAN" />
        <Career name="SORCERER" />
        <Career name="SQUIG_HERDER" />
        <Career name="WITCH_ELF" />
        <Career name="ZEALOT" />
      </Careers>
    </WARInfo>
  </UiMod>
</ModuleFile>
