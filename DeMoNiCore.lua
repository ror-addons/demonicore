--[[****************************************************************************
DeMoNiCore by DeMoN (Ken Hoffmeyer, demon AT demonicstudios DOT com)

This addon, and all original code contained in the files listed below at a), 
is copyright by Ken Hoffmeyer with all rights reserved.  The code may not be
reused or modified without express consent of the author.  You may create new
original addons that make use of this addon, however you may NOT create an addon
that uses it to circumvent the Destruction-only nature of other addons created
by the author (AntiHealer, AntiWP).

a) List of copyrighted files
DeMoNiCore.lua
DeMoNiCore.mod
****************************************************************************--]]

if (DeMoNiCore) then return end
DeMoNiCore = {}

--[[************************* Variable Declaration ****************************
****************************************************************************--]]
-- Global constants
DeMoNiCore.OTHER_HEALS = "Other Heals"
DeMoNiCore.OUTGOING_HEALS = "Outgoing Heals"
DeMoNiCore.OUTGOING_DAMAGE = "Outgoing Damage"
DeMoNiCore.INCOMING_DAMAGE = "Incoming Damage"
DeMoNiCore.EMPTY_SET = {}

-- Local constants
local OTHER_HITS = SystemData.ChatLogFilters.OTHER_HITS
local YOUR_HITS = SystemData.ChatLogFilters.YOUR_HITS
local YOUR_HEALS = SystemData.ChatLogFilters.YOUR_HEALS
local YOUR_DMG_FROM_PC = SystemData.ChatLogFilters.YOUR_DMG_FROM_PC

local VERSION = "1.3.5.2r"
local EMPTY_STRING = L""
local EMPTY_SET = DeMoNiCore.EMPTY_SET

-- Dynamic variables
local running
local observers = EMPTY_SET
--[[***************************************************************************
*************************** End Variable Declaration ***********************--]]



--[[************************* Main Event Functions ****************************
***************************************************************************--]]
function DeMoNiCore.OnInitialize()
	running = false
  DeMoNiCore.Print("Loaded DeMoNiCore v" .. VERSION .. " by DeMoN")
end


function DeMoNiCore.OnCombatLog(updateType, filterType)
  if (updateType ~= SystemData.TextLogUpdate.ADDED) then return end

  local i, msg
  local num = TextLogGetNumEntries("Combat") - 1

  _, _, wmsg = TextLogGetEntry("Combat", num);
  msg = WStringToString(wmsg)

  DeMoNiCore.Parse(wmsg, filterType)
end


function DeMoNiCore.OnShutdown()
	DeMoNiCore.Stop()
end
--[[****************************************************************************
*************************** End Main Event Functions ***********************--]]



--[[************************** Control Functions *******************************
****************************************************************************--]]
function DeMoNiCore.Start()
	if (not running) then
		RegisterEventHandler(TextLogGetUpdateEventId("Combat"),
												 "DeMoNiCore.OnCombatLog")
		running = true
	end
end

function DeMoNiCore.Stop()
	if (running) then
		UnregisterEventHandler(TextLogGetUpdateEventId("Combat"),
                                             "DeMoNiCore.OnCombatLog")
		running = false
	end
end
--[[****************************************************************************
**************************** End Control Functions *************************--]]



--[[************************** Observer Functions ******************************
****************************************************************************--]]
function DeMoNiCore.RegisterObserver(addon, func, event, skillFilters)
	-- Build up observers data structure if each step along the way is empty/nil
	local toAdd = {["function"] = func, ["skillFilters"] = skillFilters}

	if (observers[event] ~= nil) then
		if (observers[event][addon] ~= nil) then
			table.insert(observers[event][addon], toAdd)
		else
			observers[event][addon] = {toAdd}
		end
	else
		toAdd = {[addon] = {toAdd}}
		observers[event] = toAdd
	end
	
	-- Tell it to start.  There's already a check in Start to avoid starting multiple times
	DeMoNiCore.Start()
end


function DeMoNiCore.UnregisterObserver(addon, func, event)
	if (observers[event][addon] ~= nil) then
		for _, addonInfo in ipairs(observers[event][addon]) do
			if (addonInfo["function"] == func) then
				addonInfo = nil
				
				-- Since we removed something, check and clear out any empty tables
				if (observers[event][addon] == EMPTY_SET) then
					observers[event][addon] = nil
				end
	
				if (observers[event] == EMPTY_SET) then
					observers[event] = nil
				end
	
				-- No reason to keep running when the last observer is removed
				if ((observers == EMPTY_SET) or (observers == nil)) then
					DeMoNiCore.Stop()
				end
			end
		end
	end
end


function DeMoNiCore.NotifyObservers(data)
	if (observers[data.event] == nil) then return end
	
	for addon, addonInfos in pairs(observers[data.event]) do
		for i, addonInfo in ipairs(addonInfos) do
			local filters = addonInfo.skillFilters
			if ((filters ~= nil) and (filters ~= EMPTY_SET)) then
				local found = false
				for j, filter in ipairs(filters) do
					if (filter == data.skill) then found = true end
				end
				if (not found) then 
					return
				end
			end
			
			local success, errmsg = pcall(addon[addonInfo["function"]], data)
			if (not success) then
				d(L"Error calling " .. L"." .. towstring(addonInfo["function"]) .. L" callback")
				d(towstring(errmsg))
			end
		end
	end
end
--[[****************************************************************************
**************************** End Observer Functions ************************--]]



--[[*************************** Parser Functions *******************************
****************************************************************************--]]
function DeMoNiCore.Parse(text, filterType)
  local matched, ending
  local parseString
  local ability, target, caster
  local amount = 0
	local data = {}
	
	if (filterType == OTHER_HITS) then
		parseString = L"^(.-)'s (.-) heals (.-) for (%d+) points%."
		additionalParseString = L"(%d+) overhealed"
		data["event"] = DeMoNiCore.OTHER_HEALS
		matched, ending, caster, ability, target, amount = text:find(parseString)
	elseif (filterType == YOUR_HITS) then
		parseString = L"Your (.-) hits (.-) for (%d+) damage%."
		additionalParseString = L"%((%d+) mitigated%)"
		data["event"] = DeMoNiCore.OUTGOING_DAMAGE
		data["caster"] = L"Self"
		matched, ending, ability, target, amount = text:find(parseString)
	elseif (filterType == YOUR_HEALS) then
		parseString = L"Your (.-) heals (.-) for (%d+) points%."
		additionalParseString = L"%((%d+) mitigated%)"
		data["event"] = DeMoNiCore.OUTGOING_HEALS
		data["caster"] = L"Self"
		matched, ending, ability, target, amount = text:find(parseString)
	elseif (filterType == YOUR_DMG_FROM_PC) then
		parseString = L"^(.-)'s (.-) hits you for (%d+) damage%."
		additionalParseString = L"%((%d+) mitigated%)"
		data["event"] = DeMoNiCore.INCOMING_DAMAGE
		data["target"] = L"Self"
		matched, ending, caster, ability, amount = text:find(parseString)
	end

	if (not matched) then return end

	local additional, additionalAmt = 0, 0
	local crit = false
	matched = 0
	matched, _, additionalAmt = text:find(additionalParseString, ending)

	if (matched) then additional = additionalAmt end

	-- Clean up ability
	matched = 0
	matched, _ = (ability:find(L" critically"))
	if (matched) then
		crit = true
		ability = ability:gsub(L" critically", EMPTY_STRING)
	end
	ability = ability:gsub(L"^%[", EMPTY_STRING)
	ability = ability:gsub(L"%]$", EMPTY_STRING)
	
	-- Package up the rest of the data
	data["caster"] = caster
	data["ability"] = ability
	data["target"] = target
	data["amount"] = amount
	data["additional"] = additional
	data["crit"] = crit
	
  DeMoNiCore.NotifyObservers(data)
end
--[[****************************************************************************
****************************** End Parser Functions ************************--]]



--[[************************* Utility Functions *******************************
***************************************************************************--]]
function DeMoNiCore.Print(txt)
  EA_ChatWindow.Print(towstring("[DeMoNiCore] " .. txt))
end

function DeMoNiCore.PrintWString(txt)
  EA_ChatWindow.Print(L"[DeMoNiCore] " .. txt)
end
--[[***************************************************************************
*************************** End Utility Functions *************************--]]